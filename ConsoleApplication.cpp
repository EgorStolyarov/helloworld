﻿#include <iostream>
#include <vector>
using namespace std;

class Animal
{
public:
    Animal()= default;
    virtual void Voice(){
        cout << "You hear the strange voice of unidentified animal... " << endl;
    }
};

class Wolf : public Animal
{
    void Voice() override
    {
        cout << "Suddenly you hear... WOUUUUUUUUUUUUU!!!" << endl;
    }
};

class Fox : public Animal
{
    void Voice() override
    {
        cout << "Strange noise comes from the bushes: tintintin-tin-didin-didin, tintintin-tin-didin-didin..." << endl;
    }
};

class Owl : public Animal
{
    void Voice() override
    {
        cout << "Large bird is staring at you and says: uuuuu-huuuuu!" << endl;
    }
};



int main()
{
    vector<Animal*> animals;
    animals.push_back(new Owl);
    animals.push_back(new Wolf);
    animals.push_back(new Fox);

    for (auto animal : animals)
    {
        animal->Voice();
    }
    return 0;
}